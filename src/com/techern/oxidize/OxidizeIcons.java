package com.techern.oxidize;

import com.intellij.openapi.util.IconLoader;

import javax.swing.*;

/**
 * OxidizeIcons; A class that holds a variety of {@link javax.swing.Icon}s
 *
 * @since 0.0.1
 */
public class OxidizeIcons {

    /**
     * The {@link Icon} for {@link com.techern.oxidize.language.rust.RustLanguage} source files
     *
     * @since 0.0.1
     */
    public static Icon RUST_ICON = IconLoader.getIcon("/com/techern/oxidize/icons/rust.png");

    /**
     * The {@link Icon} for {@link com.techern.oxidize.language.rust.filetypes.RustLibraryFileType}s
     *
     * @since 0.0.1
     */
    public static Icon RUST_LIBRARY_ICON = IconLoader.getIcon("/com/techern/oxidize/icons/rlib.png");

    /**
     * The {@link Icon} for {@link com.techern.oxidize.language.cargo.filetypes.CargoProjectFileType}s
     *
     * @since 0.0.1
     */
    public static Icon CARGO_ICON = IconLoader.getIcon("/com/techern/oxidize/icons/crate.png"); //TODO Replace this
    
}
