package com.techern.oxidize.language.rust.filetypes;

import com.intellij.openapi.fileTypes.FileType;
import com.intellij.openapi.vfs.VirtualFile;
import com.techern.oxidize.OxidizeIcons;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

/**
 * RustLibraryFileType; A {@link FileType} for compiled {@link com.techern.oxidize.language.rust.RustLanguage} libraries
 *
 * @since 0.0.1
 */
public class RustLibraryFileType implements FileType {

    /**
     * The instance of this {@link RustLibraryFileType}
     *
     * @since 0.0.1
     */
    public static RustLibraryFileType INSTANCE = new RustLibraryFileType();

    /**
     * Creates a new {@link RustLibraryFileType}
     *
     * @since 0.0.1
     */
    private RustLibraryFileType() {
        //Do nothing
    }

    /**
     * Gets the name of this {@link RustLibraryFileType}
     *
     * @return The name
     * @since 0.0.1
     */
    @NotNull
    @Override
    public String getName() {
        return "RLib";
    }

    /**
     * Gets the description of this {@link RustLibraryFileType}
     *
     * @return The description
     * @since 0.0.1
     */
    @NotNull
    @Override
    public String getDescription() {
        return "Compiled Rust Library";
    }

    /**
     * Gets the default extension for {@link RustLibraryFileType}s
     *
     * @return The default extension
     * @since 0.0.1
     */
    @NotNull
    @Override
    public String getDefaultExtension() {
        return "rlib";
    }

    /**
     * Gets the {@link Icon} for this {@link RustLibraryFileType}
     *
     * @return The {@link Icon}
     * @since 0.0.1
     */
    @Nullable
    @Override
    public Icon getIcon() {
        return OxidizeIcons.RUST_LIBRARY_ICON;
    }

    /**
     * Checks to see if this {@link RustLibraryFileType} is binary (which it is)
     *
     * @return {@code true} since, it is binary
     * @since 0.0.1
     */
    @Override
    public boolean isBinary() {
        return true;
    }

    /**
     * Checks to see if this {@link RustLibraryFileType} is read-only (which it is not)
     *
     * @return {@code true} since it is not
     * @since 0.0.1
     */
    @Override
    public boolean isReadOnly() {
        return false;
    }

    /**
     * Attempts to get the character set
     *
     * @param virtualFile The virtual file
     * @param bytes I'm assuming the bytes contained within
     * @return Null, since this is binary
     * @since 0.0.1
     */
    @Nullable
    @Override
    public String getCharset(VirtualFile virtualFile, byte[] bytes) {
        return null;
    }
}
