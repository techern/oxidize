package com.techern.oxidize.language.rust.filetypes;

import com.intellij.openapi.fileTypes.ExtensionFileNameMatcher;
import com.intellij.openapi.fileTypes.FileTypeConsumer;
import com.intellij.openapi.fileTypes.FileTypeFactory;

/**
 * A {@link com.intellij.openapi.fileTypes.FileTypeFactory} for the {@link com.techern.oxidize.language.rust.RustLanguage}
 *
 * @since 0.0.1
 */
public class RustFileTypeFactory extends FileTypeFactory {

    /**
     * Creates the {@link com.intellij.openapi.fileTypes.FileType}s required by the {@link com.techern.oxidize.language.rust.RustLanguage}
     *
     * @param fileTypeConsumer The {@link FileTypeConsumer}
     * @since 0.0.1
     */
    @Override
    public void createFileTypes(FileTypeConsumer fileTypeConsumer) {
        fileTypeConsumer.consume(RustSourceFileType.INSTANCE, new ExtensionFileNameMatcher("rs"));
        fileTypeConsumer.consume(RustLibraryFileType.INSTANCE, new ExtensionFileNameMatcher("rlib"));
    }
}
