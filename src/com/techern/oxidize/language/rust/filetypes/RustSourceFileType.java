package com.techern.oxidize.language.rust.filetypes;

import com.intellij.openapi.fileTypes.LanguageFileType;
import com.techern.oxidize.OxidizeIcons;
import com.techern.oxidize.language.rust.RustLanguage;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

/**
 * RustSourceFileType; An extension of {@link LanguageFileType} for {@link com.techern.oxidize.language.rust.RustLanguage} source files
 *
 * @since 0.0.1
 */
public class RustSourceFileType extends LanguageFileType {

    /**
     * The instance of this {@link RustSourceFileType}
     *
     * @since 0.0.1
     */
    public static final RustSourceFileType INSTANCE = new RustSourceFileType();

    /**
     * Creates a new {@link RustSourceFileType}
     *
     * @since 0.0.1
     */
    private RustSourceFileType() {
        super(RustLanguage.INSTANCE);
    }

    /**
     * Gets the name of this {@link RustSourceFileType}
     *
     * @return The name of this {@link RustSourceFileType}
     * @since 0.0.1
     */
    @NotNull
    @Override
    public String getName() {
        return "Rust Source";
    }

    /**
     * Gets a short description of this {@link RustSourceFileType}
     *
     * @return A short description of this {@link RustSourceFileType}
     * @since 0.0.1
     */
    @NotNull
    @Override
    public String getDescription() {
        return "A Rust source file";
    }

    /**
     * Gets the default file extension of this {@link RustSourceFileType}
     *
     * @return The default file extension
     * @since 0.0.1
     */
    @NotNull
    @Override
    public String getDefaultExtension() {
        return "rs";
    }

    /**
     * Gets the default icon of this {@link RustSourceFileType}
     *
     * @return The default icon
     * @since 0.0.1
     */
    @Nullable
    @Override
    public Icon getIcon() {
        return OxidizeIcons.RUST_ICON;
    }
}
