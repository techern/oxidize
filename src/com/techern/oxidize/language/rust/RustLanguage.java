package com.techern.oxidize.language.rust;

import com.intellij.lang.Language;

/**
 * RustLanguage; An extension of {@link com.intellij.lang.Language} for Rust
 *
 * @since 0.0.1
 */
public class RustLanguage extends Language {

    /**
     * The static instance of this {@link com.techern.oxidize.language.rust.RustLanguage}
     *
     * @since 0.0.1
     */
    public static final RustLanguage INSTANCE = new RustLanguage();

    /**
     * Creates a new {@link com.techern.oxidize.language.rust.RustLanguage} instance
     *
     * @since 0.0.1
     */
    private RustLanguage() {
        super("Rust");
    }

}
