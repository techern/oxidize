package com.techern.oxidize.language.cargo;

import com.intellij.lang.Language;

/**
 * TomlLanguage; An extension of {@link com.intellij.lang.Language} for Toml
 *
 * TODO One day move this to a TOML plugin for IDEA and build off of it
 *
 * @since 0.0.1
 */
public class TomlLanguage extends Language {

    /**
     * The static instance of this {@link com.techern.oxidize.language.cargo.TomlLanguage}
     *
     * @since 0.0.1
     */
    public static final TomlLanguage INSTANCE = new TomlLanguage();

    /**
     * Creates a new {@link com.techern.oxidize.language.cargo.TomlLanguage} instance
     *
     * @since 0.0.1
     */
    private TomlLanguage() {
        super("TOML");
    }

}
