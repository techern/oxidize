package com.techern.oxidize.language.cargo.filetypes;

import com.intellij.openapi.fileTypes.LanguageFileType;
import com.techern.oxidize.OxidizeIcons;
import com.techern.oxidize.language.cargo.TomlLanguage;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

/**
 * CargoProjectFileType; An extension of {@link LanguageFileType} for {@link com.techern.oxidize.language.cargo.TomlLanguage} source files
 *
 * @since 0.0.1
 */
public class CargoProjectFileType extends LanguageFileType {

    /**
     * The instance of this {@link CargoProjectFileType}
     *
     * @since 0.0.1
     */
    public static final CargoProjectFileType INSTANCE = new CargoProjectFileType();

    /**
     * Creates a new {@link CargoProjectFileType}
     *
     * @since 0.0.1
     */
    private CargoProjectFileType() {
        super(TomlLanguage.INSTANCE);
    }

    /**
     * Gets the name of this {@link CargoProjectFileType}
     *
     * @return The name of this {@link CargoProjectFileType}
     * @since 0.0.1
     */
    @NotNull
    @Override
    public String getName() {
        return "Cargo Project";
    }

    /**
     * Gets a short description of this {@link CargoProjectFileType}
     *
     * @return A short description of this {@link CargoProjectFileType}
     * @since 0.0.1
     */
    @NotNull
    @Override
    public String getDescription() {
        return "A Cargo project file";
    }

    /**
     * Gets the default file extension of this {@link CargoProjectFileType}
     *
     * @return The default file extension
     * @since 0.0.1
     */
    @NotNull
    @Override
    public String getDefaultExtension() {
        return "toml";
    }

    /**
     * Gets the default icon of this {@link CargoProjectFileType}
     *
     * @return The default icon
     * @since 0.0.1
     */
    @Nullable
    @Override
    public Icon getIcon() {
        return OxidizeIcons.CARGO_ICON;
    }
}
