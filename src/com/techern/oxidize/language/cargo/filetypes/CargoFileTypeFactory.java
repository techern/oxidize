package com.techern.oxidize.language.cargo.filetypes;

import com.intellij.openapi.fileTypes.ExactFileNameMatcher;
import com.intellij.openapi.fileTypes.FileTypeConsumer;
import com.intellij.openapi.fileTypes.FileTypeFactory;

/**
 * {@link CargoFileTypeFactory}; An extension of {@link FileTypeFactory} for Cargo-specific {@link com.techern.oxidize.language.cargo.TomlLanguage} files
 *
 * @since 0.0.1
 */
public class CargoFileTypeFactory extends FileTypeFactory {

    /**
     * Creates the {@link com.intellij.openapi.fileTypes.FileType}s for the {@link FileTypeConsumer}
     *
     * @param fileTypeConsumer The {@link FileTypeConsumer}
     * @since 0.0.1
     */
    @Override
    public void createFileTypes(FileTypeConsumer fileTypeConsumer) {
        fileTypeConsumer.consume(CargoProjectFileType.INSTANCE, new ExactFileNameMatcher("Cargo.toml", false));
    }
}
