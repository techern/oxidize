# Oxidize

## Release info

###License: MIT

###Latest release: 0.0.1

####New features

 * Added FileType support for Rust source and rlib files, as well as Cargo.toml files. No lexing or parsing yet

###Currently in development: 0.0.2

####New features

 * Partial lexing and parsing for Rust source files. Not yet merged into master, having some issues. May be delayed to 0.0.3

####Planned features

 * Creation of blank Rust projects with no project manager. Will use rustc
 * Creation of new Cargo-based Rust projects. 
 * Partial lexing and parsing for Cargo.toml
 * New icons. The current ones aren't the best.